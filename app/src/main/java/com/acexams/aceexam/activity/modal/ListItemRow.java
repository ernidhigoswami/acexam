package com.acexams.aceexam.activity.modal;

public class ListItemRow {


    public String name;

    public ListItemRow( String name) {

        this.name = name;

    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
