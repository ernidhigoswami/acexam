package com.acexam.aceexam.minitest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MiniTestActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mini_test)
    }
}