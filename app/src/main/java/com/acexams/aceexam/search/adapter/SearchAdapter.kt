package com.acexams.aceexam.search.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.acexams.aceexam.R
import com.acexams.aceexam.activity.QuestionBankStartActivity
import com.acexams.aceexam.activity.modal.SearchResponse
import kotlinx.android.synthetic.main.adpter_subject.view.*

class SearchAdapter(var context: Context,var list: List<SearchResponse.Data>):RecyclerView.Adapter<SearchAdapter.SearchHolder>() {
   inner class SearchHolder (itemView: View) : RecyclerView.ViewHolder(itemView){
        init {
            itemView.clickonclick.setOnClickListener{
                var intent=Intent(context,QuestionBankStartActivity::class.java)
                intent.putExtra("topicid",list[adapterPosition].id.toString())
                shareprefrences.setStringPreference(context,"questioncount",list[adapterPosition].question_count.toString())
                context.startActivity(intent)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchHolder {
        return SearchHolder(LayoutInflater.from(context).inflate(R.layout.adpter_subject, parent, false))
    }

    override fun onBindViewHolder(holder: SearchHolder, position: Int) {
        holder.itemView.describtion.text=list[position].name
    }

    override fun getItemCount(): Int {
        return list.size
    }
}